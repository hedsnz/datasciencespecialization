# experimental data

- true experiment
    - only manipulate one variable
    - control group
    - random assignment

- quasi experiment
    - same except not random assignment

- statistical control: include unbalanced covariates as additional
explanatory variables in the analysis
- e.g. if randomization hasn't resulted in a truly balanced 
distribution (maybe one group skews older than another, despite
randomisation) we need to account for these in our statistical treatment.

- how to improve quasi-experimental design
    - measure as many confounding variables as possible
    - have control group
    - use pre-test and post-test design


# confounding variables

*another variable that is associated, positively or negatively,
with the explanatory and response variables*

- example: study to see which method helps smokers quit the best
    - drugs
    - therapy
    - therapy + drugs
    - just quitting

- explanatory variable == method
- response == success or failure

- highest success == therapy + drugs
- lowest success == no therapy + no drugs

- but this is just an observational study. perhaps the people
who chose a particular method also shared some other characteristic
that made them more or less likely to quit.

maybe older people tend to choose a certain method of quitting, and
are also just more likely to quit. the data appears to suggest that
it was the method, when it might just have been the age.

so here, age is a 'lurking' or confounding variable. it is tied in 
with the method and may itself cause the response to be a success or failure.

we could control for age by studying younger and older respondents separately.

even if we did this, we can't claim causation. there might be other
lurking variables that we don't know about, e.g., level of participants'
desire to quit. maybe those that chose the drug therapy might also have
greater desire to quit than those that chose no drug.

we could interview participants and rate their desire to quit from 1 to 5,
and then look at methods separately for each desire score. but this might
not be good enough.

another variable: those who opt for neither therapy nor drugs
may tend to be lower income. and maybe those with lower income
have lower success quitting because more of their friends/family/coworkers
smoke. so socioeconomic status is another possible lurking variable.

## observational studies

- can't really *prove* causation
- but are commonly used to draw conclusions about causal connections
- care must be taken to control for the most likely lurking variables

- you might be tempted to intrepret the observed association
as causation -- this is often wrong. association != causation.

# intro to multivariate methods

- very widely used, and enable us to answer questions like
    - what is the best predictor of the relationship between
    our explanatory and response variables?
    - does variable A or variable B confound this relationship?


# linear regression

## assumptions

- normality
    - residuals are normally distributed
    - plot in a histogram to check

- linearity
    - associations between explanatory variables and
    response variable are linear (i.e., best-fitting
    regression line is a straight line)

- homoscedasticity (assumption of constant variance)
    - the variability in the response variable is the same
    at all levels of the explanatory variable
    - if you plot the modelled residuals (observed score minus
    predicted score) they should be relatively linear
    along x/y.
    - you might have heteroscedasticity if, for example,
    the spread gets wider as the value of x increases.

- independence
    - observations are not correlated with each other
    - clustered data and repeated measures data have
    correlated observations


also think about 

- multicollinearity
    - explanatory variables are highly correlated with each other
- outliers

## centering explanatory variables

- in multivariate regression, we see the relationship
between a particular explanatory variable and the response
variable, while holding other variables at a constant value of zero.
- with categorical variable with two levels, e.g., male = 1, female = 2,
we can recode female = 0
- for quantitative variables, we center them by subtracting the
mean of a variable from the value of the variable. the mean is then
equal to zero.
- we only center explanatory variables, not response.



# multiple regression

- center a quantitative variable


# polynomial regression

- not linear
- just adds a squared term into the equation:
    - `smf.ols('resp_var ~ exp_var + I(exp_var**2)', data=data).fit()`
    - `I` is the identity function, returns input i.e. `exp_var` squared
- need to center the variables first
    - `dat[var] = dat[var] - dat[var].mean()`
    - centering significantly reduces the correlation between the linear
    and quadratic (i.e. squared) variables in a polynomial regression

# evaluating model fit

- need to further evaluate our models for evidence of misspecification.
- specification is the process of developing a regression model. If a model
is correctly specified, then the residuals or error terms are not correlated
with the explanatory variables. if the data fai to meet the regression
assumptions, or if our model is missing important explanatory variables, then we have model specification error.
- we can assess violation of the assumptions of the linear regression analysis by examining model residuals. i.e., we can take a closer look at the `e` in our
regression formula, which is the error, or residual estimate.

## residual plots

- remember the intercept is the value of the response variable when
all explanatory variables are held at zero.
- if we have centered our explanatory variables, then the intercept is the value 
of the response variable at the mean of both of those (since centering results in a mean of zero).
- q-q plot plots the quantiles of the residuals estimated from the regression models vs the quantiles of a normal dist? er, so if the residuals follow a straight line, then they are normally distributed.
- `sm.qqplot(reg.resid, line='r')`


## categorical explanatory variable with more than two categories

- dummy coding or parameterization: coding categorical explanataroy
variable with more than two levels.
- reference group coding or reference group parameterization is one of
the most common ways. allows us to compare each group to a reference group.
- the default ref group is the first
`smf.ols('resp ~ exp1 + exp2 + C(cat_exp)')`

- if we want to designate the ref group ourselves (i.e. if its not the first)
`+ C(cat_exp, Treatment(reference=1))` or reference=2 etc. etc. rather than 0.

# Logistic regression

- for a binary response variable.
- code as 0 == no outcome, 1 == outcome. regardless of whether outcome is positive or negative.
`smf.logit`

## odds ratio

- odds ratio is the probability of an event occurring in one group compared
to the probability of it occurring in another group.
- it can range from 0 to infinity
- OR = 1 model statistically not significant
- OR > 1 means as explanatory variable increases, response variable increases
- OR < 1 means as explanatory variable increase, response variable decreases

it is just the natural exponentiation of our parameter estimate.

`numpy.exp(reg.params)`

interpret like this. if we have an odds ratio of 3.4 for our explanatory variable,
then, e.g., group 1 is 3.4x more likely to do x than group 2.

can also get conf int

`params = reg.params`
`conf = reg.conf_int()`
`conf['OR'] = params`
`conf.columns = ["Lower CI", "Upper CI", "OR]`
`numpy.exp(conf)`
