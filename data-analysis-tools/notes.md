- a parameter is a number that describes a population
- a statistic is a number that is computed from a sample.

# ANOVA

- Analysis of variance
- For use with a categorical explanatory and a quantitative response variable
- Null hypothesis is that sample means are equal -- i.e.,
there is no relationship between the explanatory and response variables.
- ask the question: Are the differences between the sample means due
to true differences between the population means, or are they merely
due to sampling variability?

F = variation among sample means / variation within groups

tells us to what extend the variation between sample means dominates
over the variation within groups.
so a higher F score is more likely to reject null hypothesis - the variability
among sample means is greater than the variability within groups.
p-value in context of ANOVA - probability of getting the F value
at least as large as we got, if null value were true.

For categorical variables with more than 2 levels,
a significant p-value doesn't tell us which means
are different -- it just tells us that the means are different.
To figure out which means are different, we need to do post-hoc analysis.

- type 1 error: when you incorrectly reject the null hypothesis.


# Chi-square test of independence

- used to compare a categorical predictor and categorical outcome variable
- calculates percentages of the response variable for each of the explanatory variables
- e.g., % of male drivers who drank alcohol, % of female drivers who drank alcohol.

- h0 is "there is no relationship between the two categorical variables - they are independent"

- x2 measures how far the data are from the null hypothesis.
- we have two counts for each combination of variable: the observed counts, 
and the expected counts if the null hypothesis were true.
- so the actual chi-square statistic is the sum, for each cell/variable combination,
of (observed count / expected count)2 / expected count.

- for a 2x2 case, x2 is considered large if >3.84

- the p-value for the chi-square test of independence is the probability
of getting counts like those observed, assuming that the two variables
are not related

# Pearson 

- Pearson correlation coefficient measures a linear relationship
between two quantitative variables.
- The strength of the relationship is derived from how closely the 
hold to the form. e.g., close or far from the line.
- the pearson correlation coefficient is r: the numerical measure
of a linear relationship between two quantitative variables.
- it ranges from -1 to 1.
- we can then square it: r2. r2 is therefore the fraction
of the variability of one variable that can be predicted by the other.
'how much of the variation in x is explained by y?'

# Statistical interactions

- statistical interaction describes a relationship between two variables
that is dependent on or moderated by a third variable.
- e.g., do you prefer ketchup or soy sauce? your answer depends on what
food you're eating. (sushi -- soy sauce; fish and chips -- ketchup). in this
case, the third variable (what you're eating) is the moderating variable
or the moderator.
- the effect of a moderating variable is often characterised in a study
as a statistical interaction. that is, a third variable that affects
the direction and/or strength of the relation between your explanatory
and response variables.

- another case is when there are different subgroups in a population. could
the subgroups have a moderating effect on our association of interest?